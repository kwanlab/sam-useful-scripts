import sys
from Bio import SeqIO

origin_file = sys.argv[1] #File that you want to retrieve seqs from
list_file = sys.argv[2] #List of seqs you wanna extract, each heading on a new line
output_file = origin_file.split(".")[0] + "_retrieved.faa" #Can change the suffix of the new file name if you like

wanted = list(line.rstrip("\n").split(' ')[0] for line in open(list_file))
records = (r for r in SeqIO.parse(origin_file, "fasta") if r.id in wanted)
SeqIO.write(records, output_file, "fasta")