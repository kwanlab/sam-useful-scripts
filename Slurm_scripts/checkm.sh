#!/bin/bash
#SBATCH --partition=queue
#SBATCH --time=14-00:00:00
#SBATCH -N 1 # Nodes
#SBATCH -n 1 # Tasks
#SBATCH --cpus-per-task=10
#SBATCH --mem 100000
#SBATCH --error=checkm.%J.err
#SBATCH --output=checkm.%J.out
#SBATCH --mail-user=youremail
#SBATCH --mail-type=ALL

cd /path/to/folder/above/fasta/files

checkm lineage_wf -t 10 -x fasta input_fasta_file_folder /path/to/folder/above/fasta/files/checkm_output>checkm_results.txt