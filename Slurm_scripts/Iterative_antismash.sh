#!/bin/bash
#SBATCH --partition=queue
#SBATCH --time=14-00:00:00
#SBATCH -N 1 # Nodes
#SBATCH -n 1 # Tasks
#SBATCH --cpus-per-task=10
#SBATCH --error=iterative_antismash.%J.err
#SBATCH --output=iterative_antismash.%J.out

cd /path/to/fasta/files

for genome_bin in `ls *.fasta`
do
        antismash --cpus 10 --taxon bacteria --output-dir ${genome_bin/.fasta/_antismash_output}  --genefinding-tool prodigal --fullhmmer --cb-general --cb-knownclusters ${genome_bin}
done

#Note: This script uses the conda antismash package. You'll need to install that first with this command: conda install -c bioconda antismash 