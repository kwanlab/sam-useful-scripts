#!/bin/bash
#SBATCH --partition=queue
#SBATCH -N 1 # Nodes
#SBATCH -n 1 # Tasks
#SBATCH --cpus-per-task=10
#SBATCH --error=trimmomatic.%J.err
#SBATCH --output=trimmomatic.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=samche42@gmail.com

cd /home/sam/Tools

for file in `ls *1.fastq`; do

java -jar /home/sam/Tools/Trimmomatic-0.39/trimmomatic-0.39.jar PE -baseout ${file/1.fastq/'.fastq'}  \
${file} ${file/1.fasta/2.fastq} \
ILLUMINACLIP:/home/sam/Tools/Trimmomatic-0.39/adapters/TruSeq3-PE.fa:2:30:10

done