#!/bin/bash
#SBATCH --partition=queue
#SBATCH --time=14-00:00:00
#SBATCH -N 1 # Nodes
#SBATCH -n 1 # Tasks
#SBATCH --cpus-per-task=6
#SBATCH --error=kofam.%J.err
#SBATCH --output=kofam.%J.out
#SBATCH --mail-user=samche42@gmail.com
#SBATCH --mail-type=ALL

cd /path/to/fasta/file

for genome in `ls *.fasta`
do
        barrnap -k bac ${genome} --outseq ${genome/.fasta/RNAs.faa}
done