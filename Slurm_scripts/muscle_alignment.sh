#!/bin/bash
#SBATCH --partition=queue
#SBATCH --time=14-00:00:00
#SBATCH -N 1 # Nodes
#SBATCH -n 1 # Tasks
#SBATCH --mem 100000
#SBATCH --cpus-per-task=10
#SBATCH --error=muscle_tree.%J.err
#SBATCH --output=muscle_tree.%J.out
#SBATCH --mail-user=samche42@gmail.com
#SBATCH --mail-type=ALL

cd /path/to/faa/files

for protein_file in `ls *.faa`
do
        muscle -in ${protein_file} -out ${protein_file/.faa/.aln}
done