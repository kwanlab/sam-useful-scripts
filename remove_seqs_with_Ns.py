#Usage python3 remove_seqs_with_Ns.py yourfile.fasta > newfile.fasta

import sys
from Bio import SeqIO

for record in SeqIO.parse(sys.argv[1], "fasta"):
        if record.seq.count('N') == 0:
                print(record.format("fasta"))